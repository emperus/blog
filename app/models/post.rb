class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body

	def initialize(attributes=nil)
		attr_with_defaults = { :title => '', :body => ''}.merge( attributes.nil? ? {} : attributes )
		super(attr_with_defaults)
	end
end

