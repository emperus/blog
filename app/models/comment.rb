class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :post_id
	validates_presence_of :body

	def initialize(attributes=nil)
	  attr_with_defaults = { :post_id => 0, :body => ''}.merge( attributes.nil? ? {} : attributes )
	  super(attr_with_defaults)
	end
end
